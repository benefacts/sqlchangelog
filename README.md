# SqlChangeLog

Automated SQL Changelog. Lets you know what has changed in INKEXMAST on a daily basis.

This repository is used by Jenkins. 

A script is run to script out INKEXMAST - tables, sprocs, roles etc.

Jenkins commits this script (if there are changes).
